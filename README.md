## Test work Flow
- Script takes csv file of 10k URLs
- Script instanstiates chrome webdriver and opens URLs
- Script check the URL opened is migrating to expected Application
- Script checks the URL opened in expected Applications are not returning Page Not Found! 404 errors
- Script write the current Url and existing URL to test_report.CSV 
- Script adds one column Test Status in the test_report.CSV after check the Page Not Found! 404 errors 
- Script uses concurrent.futures.ThreadPoolExecutor(n) to fasten the script execution

## Best feature

- The feature **concurrent.futures.ThreadPoolExecutor** implementation helped to fasten the execution time as with normal speed internet it took around 1 hour to complete the 10k URL tested(Subjective of your hard disc/memory). Higher RAMS increases the speed and performance!

## Challenge

- The script was not compactable with computer hardware capabilities. when tried with a high number in ThreadPoolExecutor, the performance became very slow and boot error happened to be appeared quite often and which was eventually stopping the script execution in the middle. Inorder to satisfy hardware compactibility issues, I had to compromise on minimum concurrency(low number in the ThreadPoolExecutor).
