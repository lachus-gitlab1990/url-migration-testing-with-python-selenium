import json, requests, sys, os, glob, csv, time
# import requests
# import re
# from urllib import parse
import sys
from bs4 import BeautifulSoup as bs
from datetime import datetime
# from requests_html import HTMLSession
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import concurrent.futures


path = r'C:\Users\\Documents\RedirectURL\sgsme-redirects-new-Batch4.csv'

file_time = datetime.now().strftime("%Y-%m-%d_%I-%M-%S_%p")
start_time = time.time()
path_write = r'C:\Users\lekshmimnair\Documents\RedirectURL\'BT_Url_testing_report_Batch4.csv'

options = Options()
options.add_argument('--headless')  # background task; don't open a window
options.add_argument('--disable-gpu')
options.add_argument('--no-sandbox')  # I copied this, so IDK?
options.add_argument('--disable-dev-shm-usage')  # this too


# function to call the url and checks whether it had valid content or whether it return 404


def check_url_status(initial_url,url_after_redirect,nid):
    # response = session.get(url)
    driver = webdriver.Chrome(
        executable_path=r'/Driver/chromedriver.exe',
        options=options)

    driver.get(initial_url)  # set browser to use this page
    time.sleep(5)  # let the scripts load

    title = driver.title
    redirected_url = driver.current_url
    driver.quit()

    if title.find('404') != -1 and redirected_url.find(url_after_redirect) == -1:
        status = 'Failed'
    else:
        status = 'Success'
    return {'comments': title, 'status': status,'initial_url':initial_url,'url_after_redirect':url_after_redirect,'nid':nid}


with open(path_write, 'w', newline='') as outfile:
    try:

        writer = csv.writer(outfile, delimiter=",")


        writer.writerow(["nid","Initial URL", "Redirected_url", "Comments", "Status"])

        for fname in glob.glob(path):

            with open(fname, 'r', encoding='utf-8-sig') as infile:

                d_reader = csv.DictReader(infile)
                counter = 0

                with concurrent.futures.ThreadPoolExecutor(3) as executor:


                    url_test_status = [executor.submit(check_url_status,f"%{URL}{row['alias']}",row['field_content_source_url_url'],row['nid']) for row in d_reader]


                    for f in concurrent.futures.as_completed(url_test_status):
                        try:
                            writer.writerow(
                                [f.result()['nid'], f.result()['initial_url'], f.result()['url_after_redirect'], f.result()['comments'], f.result()['status']])
                            counter += 1
                            print (f"{counter} {f.result()['url_after_redirect']}")
                        except Exception as e:
                            print(e,f.result())

        print(f"time elapsed --- {(time.time() - start_time)} seconds ---")


    except:

        print("Main error occured when writing the data to file: ", str(sys.exc_info()))
